//2023-06-25

export const formatDate = function (dateString) {
  const [year, month, day] = dateString.split("-");
  return day + "." + month + "." + year;
};

export const isPast = function (dateString) {
  const dt = new Date(dateString);
  const today = new Date();
  return dt.getTime() < today.getTime();
};
